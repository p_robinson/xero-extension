'use strict';
var should = require('should');
var Xero = require('../index.js');


var config = require('../config/config.json')['test'];

var fs = require('fs');

	var config = {	key:config.ConsumerKey, 
					secret:config.ConsumerSecret, 
					rsa_key: fs.readFileSync(config.Path, "utf-8")
				};





var xero = new Xero(config);

describe('Invoices', function(){
	this.timeout(10000);
	describe('findById', function(){
		it(' Should Find One Invoice ', function(done){
			xero.invoices.findById('0032f627-3156-4d30-9b1c-4d3b994dc921', function(err, json){
				should.not.exist(err);
	 			should.exist(json);
	 			done();
			});
		});
	});
	describe('findAll', function(){
		it(' Should Find One Invoice ', function(done){
			xero.invoices.findAll(false, function(err, json){
				should.not.exist(err);
	 			should.exist(json);
	 			done();
			});
		});
	});
	describe('bulkCreate', function(){
		it(' Should Create Multiple Invoices ', function(done){

			var invoices = [{
				Type: 'ACCREC',
				Contact: {
					ContactID: 'd6a384fb-f46f-41a3-8ac7-b7bc9e0b5efa'
				},
				DueDate: '2011-07-20',
				LineItems:[
					{Description: 'Services As Agreed', Quantity: 4, UnitAmount: 100.00, AccountCode: 200}
				],
				Status: 'DRAFT'
			},{
				Type: 'ACCREC',
				Contact: {
					ContactID: 'd6a384fb-f46f-41a3-8ac7-b7bc9e0b5efa'
				},
				DueDate: '2011-07-20',
				LineItems:[
					{Description: 'Services As Agreed', Quantity: 4, UnitAmount: 100.00, AccountCode: 200}
				],
				Status: 'DRAFT'
			}];

			xero.invoices.bulkCreate(invoices, function(err, json){
				should.not.exist(err);
	 			should.exist(json);
	 			done();

			});
			
		});
	});
	describe('create', function(){
		it(' Should Find One Invoice ', function(done){

			var invoice = {
				Type: 'ACCREC',
				Contact: {
					ContactID: 'd6a384fb-f46f-41a3-8ac7-b7bc9e0b5efa'
				},
				DueDate: '2011-07-20',
				LineItems:[
					{Description: 'Services As Agreed', Quantity: 4, UnitAmount: 100.00, AccountCode: 200}
				],
				Status: 'DRAFT'
			};
			xero.invoices.create(invoice, function(err, json){
				should.not.exist(err);
	 			should.exist(json);
	 			done();
			});
		});
	});
});