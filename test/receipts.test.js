'use strict';
var should = require('should');
var Xero = require('../index.js');


var config = require('../config/config.json')['test'];

var fs = require('fs');

	var config = {	key:config.ConsumerKey, 
					secret:config.ConsumerSecret, 
					rsa_key: fs.readFileSync(config.Path, "utf-8")
				};


var xero = new Xero(config);

describe('Items', function(){
	this.timeout(10000);
	describe('findAll', function(){
		it('Should Find All Items', function(done){
			xero.items.findAll(false, function(err, json){
				console.log(json);
				should.not.exist(err);
	 			should.exist(json);
				done();
			});
		});
	});
});