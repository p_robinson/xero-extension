'use strict';
var should = require('should');
var Xero = require('../index.js');


var config = require('../config/config.json')['test'];
console.log(config)

var fs = require('fs');

	var config = {	key:config.ConsumerKey, 
					secret:config.ConsumerSecret, 
					rsa_key: fs.readFileSync(config.Path, "utf-8")
				};



var xero = new Xero(config);


describe('Contacts', function(){
	 this.timeout(10000);


	 describe('findById', function(){
	 	it(' Should Find One Contact ', function(done){
	 		xero.contacts.findById('41a42865-f15a-4fa1-b643-47877608f557', function(err, json){
	 			should.not.exist(err);
	 			should.exist(json);
	 			json.should.have.property('ContactID', '41a42865-f15a-4fa1-b643-47877608f557');
	 			done();
	 		});
	 	});

	 });
	 describe('create', function(){
	 	 it('Should Create new Contact', function(done){

	 	var cust = {
	 		Name: 'Paul Robinson',

	 	};
	 	xero.contacts.create(cust, function(err, json){

	 		should.not.exist(json);
	 		should.exist(err);
	 		done();
	 	});
	 });

	 });
	
	
	describe('bulkCreate', function(){
		it('Should Create Multiple Customers', function(done){
		
		var custs = [{Name: 'Bilbo Bagins'}, {Name:'Sam Wise'}];

		xero.contacts.bulkCreate(custs, function(err, json){
			
			should.not.exist(err);
	 		should.exist(json);
			done();
		});
	});
	});

	describe('bulkCreateOrUpdate', function(){
		it('Should Create or Update Multiple Customers', function(done){
		
		var custs = [{Name: 'Bilbo Bagins'}, {Name:'Sam Wise'}];

		xero.contacts.bulkCreateOrUpdate(custs, function(err, json){

			should.not.exist(err);
	 		should.exist(json);
			done();
		});
	});

	});

	describe('findAll', function(){
		it('Should exist', function(done){
		
		xero.contacts.findAll(false, function(err, json){

			should.not.exist(err);
	 		should.exist(json);
			done();
		});
	});

	});

});