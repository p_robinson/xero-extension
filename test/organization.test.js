var should = require('should');
var Xero = require('../index.js');
var config = require('../config/config.json')['test'];

var fs = require('fs');

	var config = {	key:config.ConsumerKey, 
					secret:config.ConsumerSecret, 
					rsa_key: fs.readFileSync(config.Path, "utf-8")
				};


var xero = new Xero(config);

describe('Organization', function(){
	this.timeout(10000);
	describe('findAll', function(){
		it('Should Return Organization Details', function(done){
			xero.organisation.findAll(false, function(err, json){
				json.should.have.property('Id')
				done(err, json);
			});
		});
	});
});