'use strict';
var endpoints = require('./src/endpoints.js');
var Xero = require('Xero');

var XeroExtension = function(config){

	this.contacts = new endpoints.Contacts(config);
	this.invoices = new endpoints.Invoices(config);
	this.receipts = new endpoints.Receipts(config);
	this.items 	  = new endpoints.Items(config);
	this.organisation 	  = new endpoints.Organisation(config);

};

module.exports = XeroExtension;