'use strict';
var Xero = require('Xero');
var Paths = function(){

Xero.call(this, this.config.key, this.config.secret, this.config.rsa_key);

	Object.defineProperties(this, {
		'get': {
			value: function (path, modified, callback) {

    			if (modified){
        			this.oa._headers['If-Modified-Since'] = modified;
    			}
    			else if (this.oa._headers['If-Modified-Since']) {
    				delete this.oa._headers['If-Modified-Since'];

    			}
    
    			this.call('GET', path, null, callback);
			} 
		},
		'post':{
			value: function (path, body, callback) {
     			this.call('POST', path, body, callback);
			}
		},
		'put': {
			value: function (path, body, callback) {
    			this.call('PUT', path, body, callback);
			}
		},
		'patch': {
			value: function (path, body, callback) {
    			this.call('PATCH', path, body, callback);
			}
		},
		'delete': {
			value: function (path, body, callback) {
    			this.call('DELETE', path, body, callback);
			}
		}
	});
};


Paths.prototype = Object.create(Xero.prototype);
Paths.prototype.constructor = Paths;


module.exports = Paths;