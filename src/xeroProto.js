"use strict";
var path = require('path');
var Paths = require('./paths');
var queryString = require('query-string');

var XeroProto = function (endpoint, config) {
	this.endpoint = endpoint;
	Paths.call(this);
	Object.defineProperties(this, {
		'findById': {
			value: function (id, cb) {
				var self = this;
				this.get.call(this, path.join('/', this.endpoint, id), null, function(err, json){
					if (err) {
						return cb(err);
					}
					else {
						return cb(null, json.Response[self.endpoint][endpoint.slice(0, -1)]);
					}
				});
			}
		},
		'findAll': {
			value: function (opts, cb) {
				var self = this;

				var where = opts.where || {};
				var modified = opts.modified || null;
				
				this.get.call(this, path.join('/', this.endpoint) + queryString.stringify(where), modified, function(err, json){
					if (err) {
						return cb(err);
					}
					else {
						if (endpoint.charAt(endpoint.length - 1) === 's') {
							return cb(null, json.Response[self.endpoint][endpoint.slice(0, -1)]);
						}
						else {

							return cb(null, json.Response);
						}

						
					}
				});

			}
		},
		'create': {
			value: function (body, cb) {
				var self = this;
    			this.put.call(this, path.join('/', endpoint), body, function (err, json) {
					if (err) {
						return cb(err);
					}
					else{
						cb(null, json.Response[self.endpoint]);
					}
				});
			}
		},
		'bulkCreate': {
			value: function (body, cb) {
				var self = this;
    			this.put.call(this, path.join('/', this.endpoint + '?summarizeErrors=false'), body, function (err, json) {
					if (err) {
						return cb(err);
					}
					else{

						cb(null, json.Response[self.endpoint]);
					}
				});
			}
		},
		'createOrUpdate': {
			value: function (body, cb) {
				var data = {};
        		data[endpoint.slice(0, -1)] = body;

    			this.post.call(this,path.join('/', endpoint), data, function (err, json) {
					if (err) {
						return cb(err);
					}
					else{
						cb(null, json[endpoint][0]);
					}
				});
			}
		},
		'bulkCreateOrUpdate': {
			value: function (body, cb) {
				var self = this;
				this.post(path.join('/', this.endpoint), body, function (err, json) {
					if (err) {
						return cb(err);
					}
					else{
						cb(null, json.Response[self.endpoint]);
					}
				});
			}
		}
	});
};
XeroProto.prototype = Object.create(Paths.prototype);
XeroProto.prototype.constructor = XeroProto;
module.exports = XeroProto;