'use strict';
var XeroProto = require('./xeroProto');

var Contacts = function(config){
	this.config = config;
	XeroProto.call(this, 'Contacts');
};
var Items = function(config){
	this.config = config;
	XeroProto.call(this, 'Items');
};
var Invoices = function(config){
	this.config = config;
	XeroProto.call(this, 'Invoices');
};
var Receipts = function(config){
	this.config = config;
	XeroProto.call(this, 'Receipts');
};
var Organisation = function(config){
	this.config = config;
	XeroProto.call(this, 'Organisation');
};

Contacts.prototype = Object.create(XeroProto.prototype);
Contacts.prototype.constructor = Contacts;
Invoices.prototype = Object.create(XeroProto.prototype);
Invoices.prototype.constructor = Invoices;
Items.prototype = Object.create(XeroProto.prototype);
Items.prototype.constructor = Items;
Receipts.prototype = Object.create(XeroProto.prototype);
Receipts.prototype.constructor = Receipts;
Organisation.prototype = Object.create(XeroProto.prototype);
Organisation.prototype.constructor = Organisation;
module.exports.Organisation = Organisation;
module.exports.Contacts = Contacts;
module.exports.Invoices = Invoices;
module.exports.Receipts = Receipts;
module.exports.Items = Items;